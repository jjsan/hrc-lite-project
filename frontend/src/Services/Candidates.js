import {candidateSelected, candidatesFetched} from "../Redux/actions";
import createObjectCandidate from "../Functions/CreateObjects";
import store from '../Redux/store';
import {getCandidateIdByArrayIndex} from "../Functions/getCandidateIdByArrayIndex";

const candidateApi = '/api/candidates'

export async function fetchCandidates() {
  let response = await fetch(candidateApi + `/`);
  let responseBody = await response.json();

  store.dispatch(candidatesFetched(responseBody))

  if (responseBody.length > 0) {
    store.dispatch(candidateSelected(responseBody[0]));
  }
}

export async function createCandidate(candidate) {
  let candidateToCreate = createObjectCandidate(
    '0',
    candidate.name,
    candidate.age,
    candidate.hired,
    candidate.technologies
  )

  return (await fetch(candidateApi + '/',
    {
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      body: JSON.stringify(candidateToCreate, ['id', 'name', 'age', 'hired', 'technologies'])
    }
  )).json();
}

export async function updateCandidate(updatedCandidate) {
  let result = await fetch(candidateApi + '/',
  {
    method: 'PUT',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(updatedCandidate, ['id', 'name', 'age', 'hired', 'technologies'])
  }
  );

  let appStore = store.getState();
  let updatedCandidates = appStore.candidatesReducer.data.candidates;
  let index = getCandidateIdByArrayIndex(updatedCandidate.id, updatedCandidates);

  updatedCandidates[index] = updatedCandidate;
  store.dispatch(candidatesFetched(updatedCandidates));

  return result;
}

export async function deleteCandidate(id) {
  let result = await fetch(candidateApi + `/${id}`,
    {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }
  );

  let appStore = store.getState();
  let updatedCandidates = appStore.candidatesReducer.data.candidates
    .filter(candidate => candidate.id !== parseInt(id));

  store.dispatch(candidatesFetched(updatedCandidates));

  if (updatedCandidates.length > 0) {
    store.dispatch(candidateSelected(updatedCandidates[0]));
  }

  return result;
}

