import store from "../Redux/store";
import {technologiesFetched} from "../Redux/actions";

export async function fetchTechnologies() {
  let response = await fetch('/api/technologies/');
  let responseBody = await response.json();

  store.dispatch(technologiesFetched(responseBody));
}