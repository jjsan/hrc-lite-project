import {
  CANDIDATE_SELECTED,
  CANDIDATES_FETCHED,
  FETCHING_DATA,
  TECHNOLOGIES_FETCHED
} from "./actions";
import {combineReducers} from "redux";
import {defaultFetchingValue, defaultTechnologies, initialCandidate} from "../Constants/Constants";

const candidateSelectedReducer = (candidate = initialCandidate, action)  => {
  if (action.type === CANDIDATE_SELECTED) {
    return {candidate: action.payload.candidate};
  }

  return candidate;
};

const candidatesReducer = (candidates = initialCandidate, action) => {
  if (action.type === CANDIDATES_FETCHED) {
    return {...candidates, data: action.payload};
  }

  return candidates;
};

const technologiesReducer = (technologies = defaultTechnologies, action) => {
  if (action.type === TECHNOLOGIES_FETCHED) {
    return {...technologies, data: action.payload};
  }

  return technologies;
};

const fetchingDataReducer = (fetching = defaultFetchingValue, action) => {
  if (action.type === FETCHING_DATA) {
    return {fetching: action.payload.fetching};
  }

  return fetching;
};

export default combineReducers(
  {
    candidateSelectedReducer,
    candidatesReducer,
    technologiesReducer,
    fetchingDataReducer
  });