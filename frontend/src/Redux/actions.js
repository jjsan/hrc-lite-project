export const CANDIDATE_SELECTED = 'CANDIDATE_SELECTED'
export const CANDIDATES_FETCHED = 'CANDIDATES_FETCHED'
export const TECHNOLOGIES_FETCHED = 'TECHNOLOGIES_FETCHED'
export const FETCHING_DATA = 'FETCHING_DATA';

export const candidateSelected = (candidate) => {
  return { type: CANDIDATE_SELECTED, payload: {candidate} }
}

export const candidatesFetched = (candidates) => {
  return { type: CANDIDATES_FETCHED, payload: {candidates} }
}

export const technologiesFetched = (technologies) => {
  return { type: TECHNOLOGIES_FETCHED, payload: {technologies} }
}

export const fetchingData = (fetching) => {
  return {type: FETCHING_DATA, payload: {fetching}}
}