import React, {Component} from 'react';
import './Candidates.css';
import CandidateSearch from "./Containers/CandidateSearch";
import NewCandidate from "./Containers/NewCandidate";
import store from "./Redux/store";
import {fetchingData} from "./Redux/actions";
import {Provider} from "react-redux";
import DetailPage from "./Containers/DetailPage";
import {fetchCandidates} from "./Services/Candidates";
import {fetchTechnologies} from "./Services/Technologies";

class CandidatesMainPage extends Component {
  state = {
    loadingData: true,
  };

  componentDidMount() {
    store.dispatch(fetchingData(true));

    fetchCandidates()
      .then(() => fetchTechnologies())
      .then(() => store.dispatch(fetchingData(false)))
      .then(() => this.setState({loadingData: false}));
  }

  render() {
    if (this.state.loadingData)
      return (<div className="App">
        <header className="App-page">
          <h1>
            Loading Data
          </h1>
        </header>
      </div>);
    else
      return (
        <div className="App">
          <header className="App-page">
            <h1>
              Candidates
            </h1>
            <Provider store={store}>
              <NewCandidate/>
              <br/>
              <DetailPage/>
              <br/>
              <CandidateSearch/>
            </Provider>
          </header>
        </div>
      );
  }
}

export default CandidatesMainPage;