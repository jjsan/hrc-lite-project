import React, {Component} from 'react';
import {fetchingData} from "../Redux/actions";
import {store} from "../Redux/store";
import InputElements from "../Containers/InputElements";
import {deleteCandidate, updateCandidate} from "../Services/Candidates";

class DetailPage extends Component {
  constructor(props) {
    super(props);

    this.updateCandidateFunction = this.updateCandidateFunction.bind(this);
    this.deleteCandidateFunction = this.deleteCandidateFunction.bind(this);
  }

  updateCandidateFunction() {
    store.dispatch(fetchingData(true));

    let updatedCandidate = this.props.selectedCandidate;

    updateCandidate(updatedCandidate).then(() => {
      store.dispatch(fetchingData(false));
    })
  }

  deleteCandidateFunction() {
    if (window.confirm("Are you sure you wish to delete this candidate?")) {
      store.dispatch(fetchingData(true));

      let candidateId = this.props.selectedCandidate.id;

      deleteCandidate(candidateId).then(() => {
        store.dispatch(fetchingData(false));
      })
    }
  }

  render() {
    let isFetchingData = this.props.fetching;

    if (isFetchingData) {
      return <p>Loading candidates...</p>;
    }

    return (
      <div>
        <b>Candidate Detail</b><br/>
        <InputElements
                       fnUpdateCandidate={this.updateCandidateFunction}
                       fnDeleteCandidate={this.deleteCandidateFunction}
                       prefix="detail"
        />
      </div>
    );
  }
}

export default DetailPage