import React, {Component} from "react";

class ButtonsNew extends Component {
  render() {
    return (
      <table>
        <tbody>
        <tr align="center">
          <td>
            <button name="Clear"
                    onClick={this.props.fnClearValues}
                    className="btn-primary">
                    Clear
            </button>
          </td>
          <td>
            <button name="Create"
                    onClick={this.props.fnCreateCandidate}
                    className="btn-success">
                    Create
            </button>
          </td>
        </tr>
        </tbody>
      </table>
    )
  }
}

export default ButtonsNew