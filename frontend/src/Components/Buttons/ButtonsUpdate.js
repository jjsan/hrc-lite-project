import React, {Component} from "react";

class ButtonsUpdate extends Component {
  render() {
    return (
      <table>
        <tbody>
        <tr align="center">
          <td>
            <button name="Delete"
                    onClick={this.props.fnDeleteCandidate}
                    className="btn-danger">
                    Delete
            </button>
          </td>
          <td>
            <button name="Update"
                    onClick={this.props.fnUpdateCandidate}
                    className="btn-success">
                    Update
            </button>
          </td>
        </tr>
        </tbody>
      </table>
    )
  }
}

export default ButtonsUpdate