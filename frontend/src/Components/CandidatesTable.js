import React, {Component} from 'react';
import CandidateTableHeader from "./CandidateTableHeader";
import {deleteCandidate, fetchCandidates} from "../Services/Candidates";
import {fetchingData} from "../Redux/actions";
import store from "../Redux/store";

class CandidatesTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      candidates: []
    };

    this.deleteCandidate = this.deleteCandidate.bind(this);
    this.handleDeleteCandidate = this.handleDeleteCandidate.bind(this);
    this.showDetails = this.showDetails.bind(this);
  }

  hired(candidateHired) {
    return candidateHired ?
      (<input type="checkbox" defaultChecked disabled/>)
      :
      (<input type="checkbox" disabled/>)
  }

  showDetails(id) {

  }

  handleDeleteCandidate(event) {
    store.dispatch(fetchingData(true));
    deleteCandidate(event.target.value)
      .then(() => store.dispatch(fetchingData(false)));
  }

  candidates() {
    if (this.state.candidates.length === 0)
      return (<tr>
          <td colSpan="3">None candidates...</td>
          </tr>
      );

    return (
    this.state.candidates.map(candidate =>
        <tr key={candidate.id}>
          <td>{candidate.name}</td>
          <td>{candidate.age}</td>
          <td>
            {this.hired(candidate.hired)}
          </td>
          {/*<td><button type="button" value={candidate.id} onClick={this.deleteCandidate}>Delete User</button></td>*/}
          <td><button type="button" value={candidate.id} onClick={this.showDetails}>Details</button></td>
        </tr>
      )
      )
  }

  tableHead() {
    return (
      (this.state.candidates.length !== 0) && (
      <CandidateTableHeader />
      )
    );
  }

  render() {
    if (this.props.fetching) {
      return <p>Loading candidates...</p>;
    }

    return (
      <div className="candidates-list">
        <h2>Candidates List</h2>
        <table>
          {this.tableHead()}
          {this.candidates()}
        </table>
      </div>
  );
  }
}
export default CandidatesTable;

