import React, {Component} from 'react';
import {candidateSelected, candidatesFetched, fetchingData} from "../Redux/actions";
import {store} from "../Redux/store";
import InputElements from "../Containers/InputElements";
import {initialCandidate} from "../Constants/Constants";
import createObjectCandidate from "../Functions/CreateObjects";
import {createCandidate} from "../Services/Candidates";

class DetailPage extends Component {
  constructor(props) {
    super(props);

    this.createCandidateFunction = this.createCandidateFunction.bind(this);
    this.clearValues = this.clearValues.bind(this);
  }

  candidateNameToTitleCase(str) {
    return str.replace(/\w\S*/g, function(txt){
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
  }

  createCandidateFunction(newCandidate) {
    store.dispatch(fetchingData(true));

    let newCandidateObject = createObjectCandidate(
      newCandidate.id,
      this.candidateNameToTitleCase(newCandidate.name),
      newCandidate.age,
      newCandidate.hired,
      newCandidate.technologies,
      )

    createCandidate(newCandidateObject)
      .then((createdCandidate) => {
        let updatedCandidates = this.props.candidates.data.candidates;

        updatedCandidates.push(createdCandidate);

        store.dispatch(candidatesFetched(updatedCandidates))
        store.dispatch(candidateSelected(createdCandidate));

        store.dispatch(fetchingData(false));
      }
    );
  }

  clearValues() {}

  render() {
    let isFetchingData = this.props.fetching;

    if (isFetchingData) {
      return <p>Loading candidates...</p>;
    }

    return (
      <div>
        <b>Create New Candidate</b><br/>
        <InputElements candidate={initialCandidate}
                       fnCreateCandidate={this.createCandidateFunction}
                       fnClearValues={this.clearValues}
                       prefix = "new"
        />
      </div>
    );
  }
}

export default DetailPage