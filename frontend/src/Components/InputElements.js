import React, {Component} from "react";
import createObjectCandidate, {createObjectTechnology} from "../Functions/CreateObjects";
import ButtonsNew from "./Buttons/ButtonsNew";
import ButtonsUpdate from "./Buttons/ButtonsUpdate";
import {initialCandidate} from "../Constants/Constants";
import store from "../Redux/store";
import {candidateSelected} from "../Redux/actions";

class InputElements extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newCandidate: initialCandidate
    };

    this.technologiesSelect = React.createRef();

    this.updateCandidate = this.updateCandidate.bind(this);
    this.deleteCandidate = this.deleteCandidate.bind(this)

    this.createCandidate = this.createCandidate.bind(this);

    this.changeValue = this.changeValue.bind(this);
    this.selectChange = this.selectChange.bind(this);
  }

  getTechnologySelectOption(candidate, technology)   {

    return this.getTechnologiesContainsCandidatesTechnologies(candidate, technology)
        && this.isUpdateFnDefined() ?
      <option key={technology.id} value={technology.name} selected="selected">{technology.name}</option> :
      <option key={technology.id} value={technology.name}>{technology.name}</option> ;
  }

  getHiredCheckBox() {
    return this.props.selectedCandidate.hired && this.isUpdateFnDefined() ?
      <input type="checkbox" id={this.getIdWithPrefix("CandidateHired")} onChange={this.changeValue} checked/> :
      <input type="checkbox" id={this.getIdWithPrefix("CandidateHired")} onChange={this.changeValue}/>
  }

  getTechnologiesContainsCandidatesTechnologies(candidateDetail, technology) {
    let technologyFound = false;
    let technologiesToSelect = candidateDetail.technologies;

    if (!technologiesToSelect) {
      return false;
    }

    technologiesToSelect.forEach(function found(currentTechnology) {
      if ((currentTechnology.id === technology.id) &&
        (currentTechnology.name === technology.name)) {
        technologyFound = true;
      }
    })

    return technologyFound;
  }

  changeValue(event) {
    let candidateNameToUpdate = this.isUpdateFnDefined() ?
      this.props.selectedCandidate.name : this.state.newCandidate.name;
    let candidateAgeToUpdate = this.isUpdateFnDefined() ?
      this.props.selectedCandidate.age : this.state.newCandidate.age;
    let candidateHiredToUpdate = this.isUpdateFnDefined() ?
      this.props.selectedCandidate.hired : this.state.newCandidate.hired;
    let candidateTechnologies = this.isUpdateFnDefined() ?
      this.props.selectedCandidate.technologies : this.state.newCandidate.technologies;

    let newValue = this.isHiredUpdated(event.target.id) ? event.target.checked: event.target.value;

    let prefix = this.props.prefix;

    switch (event.target.id) {
      case prefix + 'CandidateName': candidateNameToUpdate = newValue;
        break;
      case prefix + 'CandidateAge': candidateAgeToUpdate = newValue;
        break;
      case prefix + 'CandidateHired': candidateHiredToUpdate = newValue;
      break;
      default:
    }

    let updatedCandidate = createObjectCandidate(
      this.props.selectedCandidate.id,
      candidateNameToUpdate,
      candidateAgeToUpdate,
      candidateHiredToUpdate,
      candidateTechnologies
    );

    if (this.isUpdateFnDefined()) {
      store.dispatch(candidateSelected(updatedCandidate));
    } else {
      this.setState({newCandidate: updatedCandidate});
    }
  }

  isHiredUpdated(id) {
    return id === "candidateHired";
  }

  selectChange() {
    let options =  this.technologiesSelect;
    let value = [];

    let technologies = options.current;

    for (let i = 0; i < technologies.length; i++) {
      if (technologies[i].selected) {
        value.push(createObjectTechnology(i+1, technologies[i].value));
      }
    }

    let updatedCandidate = this.isUpdateFnDefined() ?
      createObjectCandidate(
      this.props.selectedCandidate.id,
      this.props.selectedCandidate.name,
      this.props.selectedCandidate.age,
      this.props.selectedCandidate.hired,
      value ) :
      createObjectCandidate(
        this.state.newCandidate.id,
        this.state.newCandidate.name,
        this.state.newCandidate.age,
        this.state.newCandidate.hired,
        value
      );

    if (this.isUpdateFnDefined()) {
      store.dispatch(candidateSelected(updatedCandidate));
    } else {
      this.setState({newCandidate: updatedCandidate});
    }
  }

  createCandidate(event) {
    event.preventDefault();
    this.props.fnCreateCandidate(this.state.newCandidate);
  }

  updateCandidate(event) {
    event.preventDefault();
    this.props.fnUpdateCandidate()
  }

  deleteCandidate(event) {
    event.preventDefault();
    this.props.fnDeleteCandidate()
  }

  buttonsToShow() {
    if (this.isUpdateFnDefined()) {
      return (
        <ButtonsUpdate
          fnUpdateCandidate={this.updateCandidate}
          fnDeleteCandidate={this.deleteCandidate}
        />
      )
    } else {
      return (
        <ButtonsNew
          fnCreateCandidate={this.createCandidate}
          fnClearValues={this.clearValues}
        />
      )
    }
  }

  isUpdateFnDefined() {
    return this.props.fnUpdateCandidate;
  }

  getIdWithPrefix(id) {
    return this.props.prefix + id;
  }

  getValue(valueId) {
    let toReturn;
    switch (valueId) {
      case 'name':
        toReturn = this.isUpdateFnDefined() ?
          this.props.selectedCandidate.name : this.state.newCandidate.name;
        break;
      case 'age':
        toReturn = this.isUpdateFnDefined() ?
          this.props.selectedCandidate.age : this.state.newCandidate.age;
        break;
      default:
    }

    return toReturn;
  }

  render() {
    let allTechnologies = this.props.technologies.data.technologies;

    if (this.props.fetching) {
      return <p>Loading candidates...</p>;
    }

    return (
      <form>
        <table className="left-table">
          <tbody>
          <tr>
            <td>
              <label id="labelName">Name</label>
            </td>
          </tr>
          <tr>
            <td>
              <input type="text"
                     id={this.getIdWithPrefix("CandidateName")}
                     ref={this.candidateName}
                     onChange={this.changeValue}
                     value={this.getValue('name')}
                     size="20"/>
            </td>
          </tr>
          <tr>
            <td>
              <label id="labelAge">Age</label>
            </td>
          </tr>
          <tr>
            <td>
              <input type="number"
                     id={this.getIdWithPrefix("CandidateAge")}
                     min="1"
                     ref={this.candidateAge}
                     value={this.getValue('age')}
                     onChange={this.changeValue}
                     size="50"/>
            </td>
          </tr>
          <tr>
            <td>
              <label onClick={this.changeValue}>Hired</label>
              {this.getHiredCheckBox()}
            </td>
          </tr>
          </tbody>
        </table>

        <table className="right-table">
          <tbody>
          <tr>
            <td>
              <label htmlFor="labelTechnologies">Technologies:</label>
            </td>
          </tr>
          <tr>
            <td>
              <select ref={this.technologiesSelect} className="multi-select"
                      onChange={this.selectChange}
                      multiple size={allTechnologies.length}>
                {
                  allTechnologies.map((technology) =>
                    this.getTechnologySelectOption(this.props.selectedCandidate, technology)
                  )};
              </select>
            </td>
          </tr>
          </tbody>
        </table>

        {this.buttonsToShow()}
      </form>
    );
  }
}

export default InputElements