import React, {Component} from "react";

class CandidateTableHeader extends Component {
  render() {
    return (
      <tr>
        <td>Name</td>
        <td>Age</td>
        <td>Hired</td>
      </tr>
    );
  }
}

export default CandidateTableHeader;