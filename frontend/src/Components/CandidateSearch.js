import React, {Component} from 'react';
import {candidateSelected} from "../Redux/actions";
import {store} from "../Redux/store";

class CandidateSearch extends Component {
  lastCandidatesLength = -1;

  constructor(props) {
    super(props);

    this.state = {
      filtered: [],
      firstPopulated: false,
      fetched: true
    };

    this.filterCandidates = this.filterCandidates.bind(this);
    this.detailCandidate = this.detailCandidate.bind(this);

    this.searchTechnology = React.createRef();
    this.searchAge = React.createRef();
    this.searchHired = React.createRef();
  }

  detailCandidate(event) {
    event.preventDefault();

    let candidateId = event.target.value;

    let selectedCandidate =
      this.state.filtered.filter(candidate => candidate.id === parseInt(candidateId))[0];

    store.dispatch(candidateSelected(selectedCandidate));
  }

  candidatesTable() {
    if (this.lastCandidatesLength !== this.props.candidates.data.candidates.length) {
      this.setState({filtered: this.props.candidates.data.candidates});
      this.lastCandidatesLength = this.props.candidates.data.candidates.length;
    }

    if (this.state.filtered.length === 0) {
      return (
        <tr>
          <td colSpan="3"><b><i>No candidates</i></b></td>
      </tr>)
    }

    return (
      this.state.filtered.map(candidate =>
      <tr key={candidate.id}>
        <td align="left">{candidate.name}</td>
        <td>{candidate.age}</td>
        <td>
          <button name="Detail" value={candidate.id} onClick={this.detailCandidate}>Detail</button>
        </td>
      </tr>
      )
    );
  }

  getAllAvailableTechnologies() {
    const unique = (value, index, self) => {
      return self.indexOf(value) === index
    };

    let candidates = this.props.candidates.data.candidates;

    return candidates
      .flatMap(candidate => candidate.technologies)
      .flatMap(technology => technology.name)
      .sort()
      .filter(unique).join(", ");
  }

  filterCandidates() {
    let searchAge = this.searchAge.current.value;
    let searchHired = this.searchHired.current.value;
    let candidates = this.props.candidates.data.candidates;

    this.setState({
      filtered:
        this.getCandidatesContainingTechnologies(candidates)
        .filter(candidate => searchAge === '' ?
          true : candidate.age === parseInt(searchAge))
        .filter(candidate => searchHired.includes('all') ?
          true : candidate.hired !== (searchHired.includes('notHired')))
    });
  }

  getCandidatesContainingTechnologies(candidates) {
    let searchTechnology = this.searchTechnology.current.value;

    return searchTechnology.toString().toLowerCase() === "" ?
      candidates :
      candidates.filter(candidate => candidate.technologies
      .some(technology => technology.name.toString().toLowerCase()
        .includes(searchTechnology.toString().toLowerCase())));
  }

  setFilteredFirstTime() {
    if (!this.state.firstPopulated) {
      this.setState(
        {
          filtered: this.props.candidates.data.candidates,
          firstPopulated: true
        }
      );
    }
  }

  render() {
    let isFetchingData = this.props.fetching;

    if (isFetchingData) {
      if (this.state.fetched) {
        this.setState(
          {
            firstPopulated: false,
            fetched: false
          }
        );
      }
      return <p>Loading candidates...</p>;
    }

    this.setFilteredFirstTime();

    return (
      <div>
        <h2>Search</h2>
        <i>[Available Technologies]<br />[{this.getAllAvailableTechnologies()}] <br /><br /></i>
        <table>
          <tbody>
          <tr>
            <td>Technology</td>
            <td>Age</td>
            <td>Hired</td>
          </tr>
          <tr>
            <td>
              <input type="text" ref={this.searchTechnology} onChange={this.filterCandidates}/>
            </td>
            <td>
              <input type="number" ref={this.searchAge}  onChange={this.filterCandidates}/>
            </td>
            <td>
              <select ref={this.searchHired} onChange={this.filterCandidates}>
                <option value="all">all</option>
                <option value="notHired">NOT Hired</option>
                <option value="hired">Hired</option>
              </select>
            </td>
          </tr>
            {this.candidatesTable()}
          </tbody>
        </table>
      </div>
    );
  }
}

export default CandidateSearch;
