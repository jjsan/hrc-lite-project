import createObjectCandidate from "../Functions/CreateObjects";
export const initialCandidate =
  createObjectCandidate(
    '0',
    "",
    18,
    false,
    []);

export const defaultTechnologies = {technologies: []};

export const defaultFetchingValue = {fetching: false};