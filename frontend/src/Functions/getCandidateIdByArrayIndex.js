import createObjectCandidate from "./CreateObjects"
import {initialCandidate} from "../Constants/Constants"

export function getCandidateIdByArrayIndex(idToFind, candidates) {
  if (!candidates || candidates.length === 0) {
    let emptyCandidate = initialCandidate.candidate[0];
    return createObjectCandidate(
      emptyCandidate.id,
      emptyCandidate.name,
      emptyCandidate.age,
      emptyCandidate.hired,
      emptyCandidate.technologies,
      );
  }

  return candidates.findIndex(
    candidate => candidate.id === parseInt(idToFind));
}