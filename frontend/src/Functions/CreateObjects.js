export default function createObjectCandidate(
  candidateId,
  candidateName,
  candidateAge,
  candidateHired,
  candidateTechnologies) {

  let candidate = {};

  return Object.create(candidate, {
    id: {
      value : candidateId
    },
    name: {
      value : candidateName
    },
    age: {
      value : candidateAge
    },
    hired: {
      value : candidateHired
    },
    technologies: {
      value : candidateTechnologies
    },
  });
}

export function createObjectTechnology(techId, techName) {
  const technology = {};

  return Object.create(technology, {
    id: {
      value :techId
    },
    name: {
      value : techName
    }
  });
}