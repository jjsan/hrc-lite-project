import React from 'react';
import { render } from '@testing-library/react';
import CandidatesMainPage from './CandidatesMainPage';

test('renders learn react link', () => {
  const { getByText } = render(<CandidatesMainPage />);
  const linkElement = getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
