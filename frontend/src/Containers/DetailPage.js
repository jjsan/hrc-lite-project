import { connect } from "react-redux";
import DetailPage from "../Components/DetailPage";
import {candidatesFetched, technologiesFetched} from "../Redux/actions";

const mapStateToProps = state => ({
  selectedCandidate: state.candidateSelectedReducer.candidate,
  technologies: state.technologiesReducer,
  fetching: state.fetchingDataReducer.fetching
});


const mapDispatchToProps = dispatch => ({
  candidatesFetched: candidates => dispatch(candidatesFetched(candidates)),
  technologiesFetched: technologies => dispatch(technologiesFetched(technologies))
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailPage)