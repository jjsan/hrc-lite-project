import { connect } from "react-redux";
import {candidatesFetched, technologiesFetched} from "../Redux/actions";
import CandidateSearch from "../Components/CandidateSearch";

const mapStateToProps = state => ({
  candidates: state.candidatesReducer,
  technologies: state.technologiesReducer,
  fetching: state.fetchingDataReducer.fetching
});


const mapDispatchToProps = dispatch => ({
  candidatesFetched: candidates => dispatch(candidatesFetched(candidates)),
  technologiesFetched: technologies => dispatch(technologiesFetched(technologies))
});

export default connect(mapStateToProps, mapDispatchToProps)(CandidateSearch)