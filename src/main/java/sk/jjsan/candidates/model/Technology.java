package sk.jjsan.candidates.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.Set;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
public class Technology {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @NonNull
  private String name;

//  @EqualsAndHashCode.Exclude
//  @ManyToMany(mappedBy = "technologies", fetch = FetchType.LAZY)
//  private Set<Candidate> candidates;

  @Override
  public String toString() {
    StringBuilder result = new StringBuilder(String.format(
        "Technology [id=%d, name='%s']",
        id, name));
//    if (candidates != null) {
//      for(Candidate candidate : candidates) {
//        result.append(String.format(
//            "Candidate[id=%d, name='%s']%n",
//            candidate.getId(), candidate.getName()));
//      }
//    }

    return result.toString();
  }
}