package sk.jjsan.candidates.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.Set;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Entity
public class Candidate {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  @NonNull
  private String name;
  private Integer age;
  private Boolean	hired;

  @EqualsAndHashCode.Exclude
  @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
  @JoinTable
  private Set<Technology> technologies;

  @Override
  public String toString() {
    StringBuilder result = new StringBuilder(String.format(
        "%nCandidate [id=%d, name='%s', age='%d'- %s]",
        id, name, age, hired ? "Hired" : "NOT Hired"));

    if (technologies != null) {
      result.append("\nTechnologies:");
      for(Technology technology : technologies) {
        result.append(String.format(
            "[%s] ",
            technology.getName()));
      }
    }

    return result.toString();
  }

}