package sk.jjsan.candidates.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import sk.jjsan.candidates.model.Candidate;
import sk.jjsan.candidates.model.Technology;
import sk.jjsan.candidates.repositories.CandidateRepository;
import sk.jjsan.candidates.repositories.TechnologyRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
class Initializer implements CommandLineRunner {

  private final CandidateRepository candidateRepository;
  private final TechnologyRepository technologyRepository;

  public Initializer(CandidateRepository candidateRepository, TechnologyRepository technologyRepository) {
    this.candidateRepository = candidateRepository;
    this.technologyRepository = technologyRepository;
  }

  @Override
  public void run(String... strings) {
    createAndSaveTechnologies();
    createAndSaveCandidates();
    addTechnologiesToCandidates();

    printData();
  }

  private void createAndSaveTechnologies() {
    ArrayList<Technology> technologies = new ArrayList<>();

    technologies.add(new Technology("C++"));
    technologies.add(new Technology("Java"));
    technologies.add(new Technology("Delphi"));
    technologies.add(new Technology("FullStack"));
    technologies.add(new Technology("React"));
    technologies.add(new Technology("ASM"));

    technologies.forEach(technologyRepository::save);
  }

  private void createAndSaveCandidates() {
    ArrayList<Candidate> candidates = new ArrayList<>();

    candidates.add(createCandidate("Peter Silny", 18 , false));
    candidates.add(createCandidate("Tomas Slaby", 25 , false));
    candidates.add(createCandidate("Ivan Velky", 33 , true));
    candidates.add(createCandidate("Ivan Hrozny", 33 , false));
    candidates.add(createCandidate("Tomas Sikovny", 44 , false));

    candidates.forEach(candidateRepository::save);
  }

  private void addTechnologiesToCandidates() {
    addTechnologiesToCandidate(1, Arrays.asList(1L,3L,5L));
    addTechnologiesToCandidate(2, Arrays.asList(1L,2L,4L));
    addTechnologiesToCandidate(3, Arrays.asList(4L,5L));
    addTechnologiesToCandidate(4, Arrays.asList(2L,3L,5L));
    addTechnologiesToCandidate(5, Arrays.asList(1L,3L,4L));
  }

  private Candidate createCandidate(String name, Integer age, Boolean hired) {
    Candidate candidate = new Candidate(name);

    candidate.setAge(age);
    candidate.setHired(hired);

    return candidate;
  }

  private void addTechnologiesToCandidate(long candidateId, List<Long> technologies) {
    Set<Technology> technologiesToAdd = new HashSet<>();

    for (Long technologyIndex: technologies) {
      technologiesToAdd.add(technologyRepository.getOne(technologyIndex));
    }

    Candidate candidate = candidateRepository.getOne(candidateId);
    candidate.setTechnologies(technologiesToAdd);

    candidateRepository.save(candidate);
  }

  private void printData() {
    candidateRepository.findAll().forEach(System.out::println);
  }
}