package sk.jjsan.candidates.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sk.jjsan.candidates.model.Candidate;
import sk.jjsan.candidates.repositories.CandidateRepository;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.util.Collection;

@RestController
@RequestMapping("/api/candidates")
public class CandidatesController {
//  private final Logger log = LoggerFactory.getLogger(CandidateController.class);
  private final CandidateRepository candidateRepository;

  public CandidatesController(CandidateRepository candidateRepository) {
    this.candidateRepository = candidateRepository;
  }

  @GetMapping("/")
  public Collection<Candidate> getCandidates() {
    return candidateRepository.findAll();
  }

  @PostMapping("/")
  public Candidate createCandidate(@Valid @RequestBody Candidate candidate) throws URISyntaxException {
    return candidateRepository.save(candidate);
  }

  @PutMapping("/")
  public Candidate updateCandidate(@Valid @RequestBody Candidate candidate) {
    return candidateRepository.save(candidate);
  }

  @DeleteMapping("/{id}")
  public HttpStatus deleteCandidate(@PathVariable Long id) {
    candidateRepository.deleteById(id);
    return HttpStatus.OK;
  }
}