package sk.jjsan.candidates.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sk.jjsan.candidates.model.Technology;
import sk.jjsan.candidates.repositories.TechnologyRepository;

import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/api/technologies")
public class TechnologiesController {
  private final TechnologyRepository technologyRepository;

  public TechnologiesController(TechnologyRepository technologyRepository) {
    this.technologyRepository = technologyRepository;
  }

  @GetMapping("/")
  Collection<Technology> getTechnologies() {
    return technologyRepository.findAll();
  }
}