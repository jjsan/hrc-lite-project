package sk.jjsan.candidates.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import sk.jjsan.candidates.model.Candidate;

public interface CandidateRepository extends JpaRepository<Candidate, Long> {
  Candidate findByName(String name);
}