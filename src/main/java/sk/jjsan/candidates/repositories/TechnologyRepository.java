package sk.jjsan.candidates.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import sk.jjsan.candidates.model.Technology;

public interface TechnologyRepository extends JpaRepository<Technology, Long> {
  Technology findByName(String name);
}