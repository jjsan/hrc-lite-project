package sk.jjsan.candidates.RestAssured;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.junit.Test;
import sk.jjsan.candidates.Utils;
import sk.jjsan.candidates.model.Candidate;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertFalse;

public class ApiTest {

  private static final String endpoint = "http://localhost:8080/api/";

  @Test
  public void getCandidatesAndCheckSize() {
    List<Candidate> candidates =
        getCandidates()
            .getBody()
            .jsonPath()
            .getList("$");

    assertThat(candidates.size() > 0);
  }

  @Test
  public void createCandidate() {
    List<Candidate> candidatesBeforeCreate =
        getCandidates()
            .getBody()
            .jsonPath()
            .getList("$");

    createNewCandidate();

    List<Candidate> candidatesAfterCreate =
        getCandidates()
            .getBody()
            .jsonPath()
            .getList("$");

    assertThat(candidatesAfterCreate.containsAll(candidatesBeforeCreate));
    assertFalse(candidatesBeforeCreate.containsAll(candidatesAfterCreate));
  }

  @Test
  public void updateCandidate() {
    List<Candidate> candidatesBeforeUpdate =
        getCandidates()
            .getBody()
            .jsonPath()
            .getList("$");

    updateCandidateValues(candidatesBeforeUpdate.size());

    List<Candidate> candidatesAfterUpdate =
        getCandidates()
            .getBody()
            .jsonPath()
            .getList("$");

    assertThat(candidatesBeforeUpdate.size() == candidatesAfterUpdate.size());
    assertFalse(candidatesAfterUpdate.containsAll(candidatesBeforeUpdate));
  }

  @Test
  public void deleteCandidate() {
    List<Candidate> candidatesBeforeDeleting =
        getCandidates()
            .getBody()
            .jsonPath()
            .getList("$");

    deleteCandidate(String.valueOf(candidatesBeforeDeleting.size()));

    List<Candidate> candidatesAfterDeleting =
        getCandidates()
            .getBody()
            .jsonPath()
            .getList("$");

    assertThat(candidatesBeforeDeleting.containsAll(candidatesAfterDeleting));
    assertFalse(candidatesAfterDeleting.containsAll(candidatesBeforeDeleting));
  }

  private Response getCandidates() {
    return given()
        .headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON)
        .when()
        .get(endpoint + "candidates")
        .then()
        .contentType(ContentType.JSON)
        .extract()
        .response();
  }

  private void createNewCandidate() {
    Candidate newCandidate = Utils.createTestCandidate();

    String json = null;

    try {
      ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
      json = ow.writeValueAsString(newCandidate);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }

    given()
        .headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON)
        .body(json)
        .when()
        .put(endpoint + "newCandidate")
        .then()
        .assertThat()
        .statusCode(200);
  }

  private void updateCandidateValues(long candidateId) {
    Candidate candidateToUpdate = Utils.createTestCandidate();
    candidateToUpdate.setId(candidateId);
    candidateToUpdate.setName("New Name");
    candidateToUpdate.setHired(true);
    candidateToUpdate.setAge(18);

    String json = "";

    try {
      ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
      json = ow.writeValueAsString(candidateToUpdate);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }

    given()
        .headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON)
        .body(json)
        .when()
        .put(endpoint + "updateCandidate")
        .then()
        .assertThat()
        .statusCode(200);
  }

  private void deleteCandidate(String candidateId) {
    given()
        .headers("Content-Type", ContentType.JSON, "Accept", ContentType.JSON)
        .when()
        .delete(endpoint + "deleteCandidate/" + candidateId)
        .then()
        .contentType(ContentType.JSON)
        .extract()
        .response();
  }
}
