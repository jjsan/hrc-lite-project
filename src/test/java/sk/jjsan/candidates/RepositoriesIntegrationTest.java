package sk.jjsan.candidates;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import sk.jjsan.candidates.model.Candidate;
import sk.jjsan.candidates.repositories.CandidateRepository;
import sk.jjsan.candidates.model.Technology;
import sk.jjsan.candidates.repositories.TechnologyRepository;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RepositoriesIntegrationTest {

  @Autowired
  private TestEntityManager entityManager;

  @Autowired
  private CandidateRepository candidateRepository;
  @Autowired
  private TechnologyRepository technologyRepository;

  @Test
  public void createCandidateByNameThenFindCandidateByName() {
    Candidate candidate = new Candidate("Jan Hladky");

    entityManager.persist(candidate);
    entityManager.flush();

    Candidate found = candidateRepository.findByName(candidate.getName());
    assertThat(found.getName()).isEqualTo(candidate.getName());
  }

  @Test
  public void createTechnologyByNameThenFindCandidateByName() {
    Technology technology =  new Technology("SpringBoot");

    entityManager.persist(technology);
    entityManager.flush();

    Technology found = technologyRepository.findByName(technology.getName());
    assertThat(found.getName()).isEqualTo(technology.getName());
  }

}