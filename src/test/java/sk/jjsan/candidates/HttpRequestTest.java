package sk.jjsan.candidates;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import sk.jjsan.candidates.controller.CandidatesController;
import sk.jjsan.candidates.controller.TechnologiesController;
import sk.jjsan.candidates.model.Candidate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class HttpRequestTest {

  @LocalServerPort
  private final int port = 8080;

  @Test
  public void testGetCandidatesListSuccess() throws URISyntaxException
  {
    RestTemplate restTemplate = new RestTemplate();

    URI uri = new URI(getUrl("candidates"));

    ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);

    //Verify request succeed
    Assert.assertEquals(200, result.getStatusCodeValue());
    Assert.assertTrue(Objects.requireNonNull(result.getBody()).contains("Peter Silny"));
  }

  @Test
  public void testAddCandidateMethodNotAllowed() throws URISyntaxException
  {
    RestTemplate restTemplate = new RestTemplate();

    URI uri = new URI(getUrl("newCandidate"));
    Candidate candidate = new Candidate("Meno Priezvisko");

    HttpHeaders headers = new HttpHeaders();

    HttpEntity<Candidate> request = new HttpEntity<>(candidate, headers);

    try
    {
      restTemplate.postForEntity(uri, request, String.class);
      Assert.fail();
    }
    catch(HttpClientErrorException ex)
    {
      //Verify bad request and missing header
      Assert.assertEquals(405, ex.getRawStatusCode());
      Assert.assertTrue(ex.getResponseBodyAsString().contains("Method Not Allowed"));
    }
  }

  @Test
  public void testUpdateCandidateNameAndReturnUpdatedCandidate() throws URISyntaxException
  {
    RestTemplate restTemplate = new RestTemplate();

    URI uri = new URI(getUrl("candidates"));

    ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);

    //Verify request succeed
    Assert.assertEquals(200, result.getStatusCodeValue());
    Assert.assertTrue(Objects.requireNonNull(result.getBody()).contains("Peter Silny"));

    uri = new URI(getUrl("updateCandidate"));

    Map<String, String> params = new HashMap<>();
    params.put("id", "1");

    Candidate updatedCandidate = new Candidate("New Name");

    restTemplate = new RestTemplate();
    restTemplate.put(String.valueOf(uri), updatedCandidate, params);

    uri = new URI(getUrl("candidates"));

    result = restTemplate.getForEntity(uri, String.class);

    //Verify request succeed
    Assert.assertEquals(200, result.getStatusCodeValue());
    Assert.assertTrue(Objects.requireNonNull(result.getBody()).contains("Peter Silny"));

  }

  private String getUrl(String url) {
    return "http://localhost:"+ port +"/api/"+url+"/";
  }

  @SpringBootTest
  public static class CandidatesApplicationTest {

    @Autowired
    private CandidatesController candidatesController;
    @Autowired
    private TechnologiesController technologiesController;

    @org.junit.jupiter.api.Test
    public void contextLoads() throws Exception {
      assertThat(candidatesController).isNotNull();
      assertThat(technologiesController).isNotNull();
    }
  }
}