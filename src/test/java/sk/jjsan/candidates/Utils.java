package sk.jjsan.candidates;

import sk.jjsan.candidates.model.Candidate;
import sk.jjsan.candidates.model.Technology;

import java.util.HashSet;
import java.util.Set;

public class Utils {

  private static final Long userId = 1L;
  private static final String userName = "John Smith";
  private static final int userAge = 35;
  private static final boolean userHired = false;
  private static final Technology technology = new Technology("C++");

  public static Candidate createTestCandidate() {
    Candidate candidate = new Candidate(userName);

    Set<Technology> technologies = new HashSet<>();
    technologies.add(technology);

    candidate.setAge(userAge);
    candidate.setHired(userHired);
    candidate.setTechnologies(technologies);

    return candidate;
  }
}
