package sk.jjsan.candidates.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import sk.jjsan.candidates.model.Candidate;
import sk.jjsan.candidates.model.Technology;
import sk.jjsan.candidates.repositories.CandidateRepository;

import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static sk.jjsan.candidates.Utils.createTestCandidate;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CandidatesControllerTest{

//  private static final String userName = "John Smith";
  private static final String newUserName = "John Watson";
  private static final int userAge = 35;
  private static final int newUserAge = 53;
  private static final boolean userHired = false;
  private static final Technology technology = new Technology("C++");

  @Mock
  private CandidateRepository candidateRepository;

  private CandidatesController candidatesController;

  @Test
  void getAllCandidates() throws URISyntaxException {
    Candidate candidate = createTestCandidate();
    candidate.setAge(userAge);
    candidate.setHired(userHired);
    candidate.setTechnologies(Collections.singleton(technology));

    List<Candidate> candidates = Collections.singletonList(candidate);

    candidatesController = new CandidatesController(candidateRepository);
    when(candidateRepository.findAll())
        .thenReturn(candidates);

    Collection<Candidate> returnedCandidate = candidatesController.getCandidates();

    assertThat(returnedCandidate.equals(candidates));
  }

  @Test
  void createCandidateAndReturnIt() throws URISyntaxException {
    Candidate candidate = createTestCandidate();
    candidate.setAge(userAge);
    candidate.setHired(userHired);
    candidate.setTechnologies(Collections.singleton(technology));

    candidatesController = new CandidatesController(candidateRepository);
    when(candidateRepository.save(any(Candidate.class)))
        .thenReturn(candidate);

    Candidate returnedCandidate = candidatesController.createCandidate(candidate);

    assertThat(returnedCandidate.getName().equals(candidate.getName()));
    assertThat(returnedCandidate.getAge().equals(candidate.getAge()));
    assertThat(returnedCandidate.getHired().equals(candidate.getHired()));
  }

  @Test
  void shouldUpdateCandidateAndReturnIt() throws URISyntaxException {
    Candidate candidate = createTestCandidate();
    candidate.setAge(newUserAge);

    candidatesController = new CandidatesController(candidateRepository);
    when(candidateRepository.save(any(Candidate.class)))
        .thenReturn(candidate);

    Candidate returnedCandidate = candidatesController.createCandidate(candidate);


    assertThat(returnedCandidate.getName().equals(candidate.getName()));
    assertThat(returnedCandidate.getAge().equals(candidate.getAge()));
    assertThat(returnedCandidate.getHired().equals(candidate.getHired()));

    candidate.setName(newUserName);
    candidate.setAge(newUserAge);

    returnedCandidate = candidatesController.updateCandidate(candidate);

    assertThat(returnedCandidate.getName().equals(candidate.getName()));
    assertThat(returnedCandidate.getAge().equals(candidate.getAge()));
    assertThat(returnedCandidate.getHired().equals(candidate.getHired()));
  }

  @Test
  void shouldDeleteCandidate() {
    candidatesController = new CandidatesController(candidateRepository);
    candidatesController.deleteCandidate(1L);

    verify(candidateRepository).deleteById(any());

  }
}