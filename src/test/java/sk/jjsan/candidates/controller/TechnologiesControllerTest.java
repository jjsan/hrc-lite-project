package sk.jjsan.candidates.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import sk.jjsan.candidates.model.Candidate;
import sk.jjsan.candidates.model.Technology;
import sk.jjsan.candidates.repositories.CandidateRepository;
import sk.jjsan.candidates.repositories.TechnologyRepository;

import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static sk.jjsan.candidates.Utils.createTestCandidate;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TechnologiesControllerTest {

  private static final Technology technology = new Technology("C++");

  @Mock
  private TechnologyRepository technologyRepository;

  private TechnologiesController technologiesController;

  @Test
  void getAllTechnologies() {
    technologiesController = new TechnologiesController(technologyRepository);

    List<Technology> technologies = Collections.singletonList(technology);

    when(technologyRepository.findAll())
        .thenReturn(technologies);

    Collection<Technology> technologiesReturned = technologiesController.getTechnologies();

    assert(technologiesReturned.equals(technologies));
  }
}